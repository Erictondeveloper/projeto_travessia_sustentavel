import 'dart:io';
import 'package:flutter/material.dart';
import 'package:travessia_sustentavel_mobile/src/page/info_Ambiental/info_Ambiental.dart';
import 'package:travessia_sustentavel_mobile/src/page/lead_page/lead_page.dart';
import 'package:travessia_sustentavel_mobile/src/page/sobre/sobre_page.dart';

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Image.asset('assets/images/splash-screen.png'),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
                gradient: LinearGradient(
                  colors: <Color>[
                    Color(0xff5F9E29),
                    Color(0xff417618),
                  ],
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.alarm, color: Color(0xff417618)),
              title: Text(
                "Horário da lancha".toUpperCase(),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.menu_book, color: Color(0xff417618)),
              title: Text(
                "Faça parte de nossos projetos".toUpperCase(),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              onTap: () {
                Navigator.push(context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => LeadPage()
                  )
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.notifications_active, color: Color(0xff417618)),
              title: Text(
                "Informativo Ambiental".toUpperCase(),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              onTap: () {
                // ignore: non_constant_identifier_names
                Navigator.push( context,
                  MaterialPageRoute(
                      builder: (_) => InformativoAmbientalPage()
                  )
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.info, color: Color(0xff417618)),
              title: Text(
                "Sobre".toUpperCase(),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              onTap: () {
                Navigator.push(context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => SobrePage()
                  )
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.exit_to_app, color: Colors.red,),
              title: Text(
                "Sair".toUpperCase(),
                style: TextStyle(
                  color: Colors.red,
                  fontSize: 16,
                  fontWeight: FontWeight.bold
                ),
              ),
              onTap: () => exit(0),
            ),
          ],
        ),
      ),
    );
  }
}
