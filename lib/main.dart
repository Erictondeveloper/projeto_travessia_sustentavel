import 'package:flutter/material.dart';
import 'package:travessia_sustentavel_mobile/src/page/animation/intro.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // drawer: MainDrawer(),
    return MaterialApp(
      title: 'Travessia Sustentável',
      theme: ThemeData(
        backgroundColor: Color(0xff5F9E29),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      home: SplashScreen(),
    );
  }
}
