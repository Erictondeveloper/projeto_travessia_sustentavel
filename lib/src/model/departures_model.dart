import 'dart:async';

class Departures {
  int id;
  String boatName;
  String price;
  String portLeftName;
  String portRightName;
  String status;
  String timeLeftPlan;
  String timeRightPlan;

  Departures(
      {this.id,
      this.boatName,
      this.price,
      this.portLeftName,
      this.portRightName,
      this.status,
      this.timeLeftPlan,
      this.timeRightPlan});

  Departures.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    boatName = json['boatName'];
    price = json['price'];
    portLeftName = json['portLeftName'];
    portRightName = json['portRightName'];
    status = json['status'];
    timeLeftPlan = json['timeLeftPlan'];
    timeRightPlan = json['timeRightPlan'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['boatName'] = this.boatName;
    data['price'] = this.price;
    data['portLeftName'] = this.portLeftName;
    data['portRightName'] = this.portRightName;
    data['status'] = this.status;
    data['timeLeftPlan'] = this.timeLeftPlan;
    data['timeRightPlan'] = this.timeRightPlan;
    return data;
  }
}
