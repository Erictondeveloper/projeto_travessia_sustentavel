class InformativeModel {
  int id;
  String title;
  String image;
  String subtitle;
  //String video;
  int order;
 

  InformativeModel(
      {this.id,
      this.title,
      this.image,
      this.subtitle,
      //this.video,
      this.order,
      });

  InformativeModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    image = json['image'];
    subtitle = json['subtitle'];
    //video = json['video'];
    order = json['order'];
    
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['image'] = this.image;
    data['subtitle'] = this.subtitle;
   // data['video'] = this.video;
    data['order'] = this.order;
    
    return data;
  }
}
