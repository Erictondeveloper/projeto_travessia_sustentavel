import 'package:flutter/cupertino.dart';

class CustomClipperBackground extends CustomClipper<Path>{

  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0, size.height / 1.75);
    var firstStart = Offset(size.width / 6, size.height);
    var firstEnd = Offset(size.width / 2, size.height - 50.0);
    path.quadraticBezierTo(firstStart.dx, firstStart.dy, firstEnd.dx, firstEnd.dy);
    var secondStart = Offset(size.width + 40 - (size.width / 5), size.height - 110);
    var secondEnd = Offset(size.width, size.height - 40);
    path.quadraticBezierTo(secondStart.dx, secondStart.dy, secondEnd.dx, secondEnd.dy);
    path.lineTo(size.width,0);
    path.close();
    return path;
  }
  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}