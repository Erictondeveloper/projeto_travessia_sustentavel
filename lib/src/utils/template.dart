import 'package:flutter/material.dart';

import 'constants.dart';
import 'custom_background_clipper.dart';

class Template extends StatefulWidget {
  final Widget widget;
  final String title;

  Template(this.title, this.widget);

  @override
  _TemplateState createState() => _TemplateState();
}

class _TemplateState extends State<Template> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF5F9E29),
        centerTitle: true,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            color: kPrimaryColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Container(
                  color: kPrimaryColor,
                  child: ClipPath(
                    clipper: CustomClipperBackground(),
                    child: Container(
                        height: 160,
                        color: kClipperBackgroundColor,
                        padding: EdgeInsets.fromLTRB(35, 4, 20, 50),
                        child: Text(
                          widget.title,
                          style: kTitleAppBarStyle,
                          textAlign: TextAlign.center,
                        )),
                  ),
                ),
                widget.widget,
              ],
            ),
          ),
        ),
      ),
    );
  }
}
