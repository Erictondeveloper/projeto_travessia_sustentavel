import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

const urlApi = 'https://travessia-sustentavel-api-pipe.herokuapp.com';

const kClipperBackgroundColor = Color(0xFF5F9E29);
const kPrimaryColor = Color(0xFFD1CFD7);

const kTitleAppBarStyle = TextStyle(
    color: Color(0xffFFFFFF),
    fontSize: 24.0,
    fontFamily: 'Roboto',
    fontWeight: FontWeight.bold,
    decoration: TextDecoration.none);

const kSubtitleAppBarStyle = TextStyle(
  color: Color(0xffFFFFFF),
  fontWeight: FontWeight.w700,
  fontSize: 15.0,
);


