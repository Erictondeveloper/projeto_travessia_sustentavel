import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:travessia_sustentavel_mobile/src/model/Informative_model.dart';
import 'package:travessia_sustentavel_mobile/src/utils/constants.dart';
import 'package:travessia_sustentavel_mobile/src/utils/template.dart';

class InformativoAmbientalPage extends StatefulWidget {
  @override
  _InformativoAmbientalPage createState() => _InformativoAmbientalPage();
}

class _InformativoAmbientalPage extends State<InformativoAmbientalPage> {
  List<InformativeModel> filteredUsers = List();

  List<InformativeModel> _cards = List<InformativeModel>();

  Future<List<InformativeModel>> _getInformative() async {
    var url = "$urlApi/informative/active";
    var response = await http.get(url);
    var cards = List<InformativeModel>();

    if (response.statusCode == 200) {
      var cardsJson = json.decode(response.body);
      for (var noteJson in cardsJson) {
        if (noteJson['image'] != null) {
          cards.add(InformativeModel.fromJson(noteJson));
        }
      }
    }
    return cards;
  }

  @override
  void initState() {
    _getInformative().then((value) {
      setState(() {
        _cards.addAll(value);
        filteredUsers = _cards;
      });
    });
    super.initState();
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 1));

    setState(() {
      _getInformative();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Template(
      'Informativo Ambiental'.toUpperCase(),
      Expanded(
        child: FutureBuilder(
          future: _getInformative(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
              case ConnectionState.none:
                return Container(
                    width: 50,
                    height: 50,
                    alignment: Alignment.topCenter,
                    child: filteredUsers.isEmpty
                        ? CircularProgressIndicator(
                            strokeWidth: 5.0,
                          )
                        : null);
              default:
                return Container(
                  margin: EdgeInsets.only(bottom: 68),
                  child: RefreshIndicator(
                    onRefresh: _refresh,
                    child: ListView.builder(
                      itemCount: filteredUsers.length,
                      itemBuilder: (context, index) {
                        return Center(
                          child: Container(
                            margin: EdgeInsets.only(bottom: 10),
                            width: double.infinity,
                            padding: EdgeInsets.fromLTRB(8, 0, 8, 8),
                            child: Card(
                              color: Color(0xFF46731F),
                              semanticContainer: true,
                              clipBehavior: Clip.antiAliasWithSaveLayer,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20)),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Image(
                                    image: (filteredUsers[index].image != '')
                                        ? NetworkImage(
                                            "$urlApi/upload/${filteredUsers[index].image}")
                                        : AssetImage("assets/images/test.png"),
                                    fit: BoxFit.fill,
                                    height: 145,
                                    width: double.infinity,
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                        left: 15.0,
                                        top: 15.0,
                                        right: 0.0,
                                        bottom: 15.0),
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(filteredUsers[index].title,
                                            style: kTitleAppBarStyle.copyWith(
                                                fontSize: 20)),
                                        Text(filteredUsers[index].subtitle,
                                            style: kSubtitleAppBarStyle),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                );
            }
          },
        ),
      ),
    );
  }
}
