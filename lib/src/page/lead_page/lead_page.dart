import 'package:flutter/material.dart';
import 'package:travessia_sustentavel_mobile/src/utils/template.dart';
import 'package:url_launcher/url_launcher.dart';

import 'widget_lead.dart';

class LeadPage extends StatefulWidget {
  @override
  _LeadPageState createState() => _LeadPageState();
}

class _LeadPageState extends State<LeadPage> {
  @override
  Widget build(BuildContext context) {
    return Template(
      'Participe dos Projetos de Responsabilidade Social'.toUpperCase(),
      Column(
        children: [
          WidgetLead('PROJETO TRAVESSIA SUSTENTÁVEL', 'INSCREVA-SE AQUI',
              'https://bit.ly/TravessiaUNN'),
          WidgetLead('NOSSO PRIMEIRO CANAL DO YOUTUBE', 'CLIQUE AQUI',
              'https://www.youtube.com/channel/UCgJPAnvSJ8Z0wynyd1RHINw'),
          Card(
            margin: EdgeInsets.only(top: 21),
            elevation: 7,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(15.0)),
            ),
            child: InkWell(
              onTap: () => _link('https://bit.ly/TravessiaUNN'),
              child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
                child: Image.asset(
                  'assets/images/banner_lead.jpg',
                  width: 340,
                  height: 200,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _link(String url) async {
    if (await canLaunch(url)) {
      launch(url);
    } else {
      throw 'Página não está acessivel!';
    }
  }
}
