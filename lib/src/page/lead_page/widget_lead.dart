import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class WidgetLead extends StatelessWidget {
  final String title;
  final String btText;
  final String urlLead;

  WidgetLead(this.title, this.btText, this.urlLead);

  _link(String url) async {
    if (await canLaunch(url)) {
      launch(url);
    } else {
      throw 'Página não está acessivel!';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            title,
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          Container(
            width: 200,
            margin: EdgeInsets.fromLTRB(0, 12, 0, 12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25),
              color: Color(0xff5F9E29),
            ),
            child: FlatButton(
              child: Text(btText,
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold)),
              onPressed: () => _link(urlLead),
            ),
          ),
        ],
      ),
    );
  }
}
