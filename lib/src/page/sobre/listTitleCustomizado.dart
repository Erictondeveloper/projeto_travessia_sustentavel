import 'package:flutter/material.dart';

class ListTitleCustomizado extends StatefulWidget {
  IconData icon;
  String text;
  Function onTap;

  ListTitleCustomizado(this.icon, this.text, this.onTap);

  @override
  _ListTitleCustomizadoState createState() => _ListTitleCustomizadoState();
}

class _ListTitleCustomizadoState extends State<ListTitleCustomizado> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 5.0, 8.0, 0),
      child: Container(
        child: InkWell(
          onTap: widget.onTap,
          child: Container(
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(widget.icon, color: Color(0xff417618)),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        widget.text,
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
