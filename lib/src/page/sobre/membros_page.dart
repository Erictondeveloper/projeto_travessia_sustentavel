import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class MembrosPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffffffff),
      appBar: AppBar(
        backgroundColor: Color(0xff5F9E29),
        title: Text(
          'Desenvolvedores',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
        centerTitle: true,
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: const EdgeInsets.only(top: 40, left: 20, right: 20),
        child: ListView(
          children: <Widget>[
            Text(
              'O aplicativo Travessia Sustentável foi desenvolvido por acadêmicos dos cursos de Sistema da Informação, Ciência da Computação, Engenharia da Computação e CST Análise e Desenvolvimento de Sistemas do Centro Universitário do Norte – Uninorte.  Com a orientação do corpo Docente dos cursos de Computação.',
              textAlign: TextAlign.justify,
              style: TextStyle(
                fontSize: 17,
                color: Color(0xff000000),
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 40),
            Image.asset('assets/images/logouninorte.jpg'),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(Icons.school, color: Color(0xff417618)),
                Text(
                  'Rua 10 de julho, 873 - Centro-Manaus-AM',
                  style: TextStyle(
                      fontSize: 15,
                      color: Color(0xff443D3D),
                      fontWeight: FontWeight.w500),
                ),
              ],
            ),
           SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(Icons.phone, color: Color(0xff417618)),
                Text(
                  '3212-5000',
                  style: TextStyle(
                      fontSize: 15,
                      color: Color(0xff443D3D),
                      fontWeight: FontWeight.w500),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(Icons.smartphone, color: Color(0xff417618)),
                GestureDetector(
                  child: Text(
                    '(92) 99977-6069',
                    style: TextStyle(
                        fontSize: 15,
                        color: Color(0xff443D3D),
                        fontWeight: FontWeight.w500),
                  ),
                  onTap: () => _link('https://wa.me/5592999776069'),
                ),
              ],
            ),
            SizedBox(height: 10),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Icon(Icons.language, color: Color(0xff417618)),
                GestureDetector(
                  child: Text(
                    'www.uninorte.com.br',
                    style: TextStyle(
                        fontSize: 15,
                        color: Color(0xff443D3D),
                        fontWeight: FontWeight.w500),
                  ),
                  onTap: () => _link('http://www.uninorte.com.br'),
                ),
              ],
            ),
            SizedBox(
              height: 16,             
            ),
          ],
        ),
      ),
    );
  }

  _link(String url) async {
    if (await canLaunch(url)) {
      launch(url);
    } else {
      throw 'Página não está acessivel!';
    }
  }
}
