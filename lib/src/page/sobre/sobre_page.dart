import 'package:flutter/material.dart';
import 'dart:io';
import 'package:travessia_sustentavel_mobile/src/page/Sobre/listTitleCustomizado.dart';
import 'package:travessia_sustentavel_mobile/src/page/Sobre/membros_page.dart';
import 'package:travessia_sustentavel_mobile/src/utils/constants.dart';
import 'package:travessia_sustentavel_mobile/src/utils/template.dart';
import 'package:url_launcher/url_launcher.dart';

class SobrePage extends StatefulWidget {
  const SobrePage({
    Key key,
  }) : super(key: key);

  @override
  _SobrePageState createState() => _SobrePageState();
}

class _SobrePageState extends State<SobrePage> {

  @override
  Widget build(BuildContext context) {
    return Template(
      'Sobre',
      Container(
        child: Expanded(
          child: ListView(
            shrinkWrap: true,
            children: <Widget>[
              ListTitleCustomizado(
                  Icons.phone_iphone, 'Versão 1.0.0', () => ('')),
              ListTitleCustomizado(Icons.language, 'WebSite',
                  () => _link('http://acamdaf.com.br/')
              ),
              ListTitleCustomizado(Icons.emoji_people, 'Desenvolvedores',
                  () => { Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => MembrosPage()
                  ))
                }
              ),
            ],
          ),
        ),
      ),
    );
  }

  _link(String url) async {
    if (await canLaunch(url)) {
      launch(url);
    } else {
      throw 'Página não está acessivel!';
    }
  }
}
