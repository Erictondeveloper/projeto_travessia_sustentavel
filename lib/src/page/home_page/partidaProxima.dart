import 'package:flutter/material.dart';
import 'package:travessia_sustentavel_mobile/src/model/departures_model.dart';
import 'package:travessia_sustentavel_mobile/src/utils/constants.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../../../mainDrawer.dart';

class PartidaProximaPage extends StatefulWidget {
  @override
  _PartidaProximaPageState createState() => _PartidaProximaPageState();
}

class _PartidaProximaPageState extends State<PartidaProximaPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        backgroundColor: Color(0xff5F9E29),
        title: Text("Horários das Lanchas".toUpperCase(),
            style: TextStyle(
              fontSize: 16,
            )),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.place),
              onPressed: () {
                _link(
                    'https://www.google.com/maps/place/Marina+Do+Davi/@-3.0531338,-60.1114119,17z/data=!4m12!1m6!3m5!1s0x926c115d63aaee4d:0x1e9bc28c2950964!2sMarina+Do+Davi!8m2!3d-3.0531338!4d-60.1092232!3m4!1s0x926c115d63aaee4d:0x1e9bc28c2950964!8m2!3d-3.0531338!4d-60.1092232');
              }),
        ],
      ),
      body: CardHour(),
    );
  }

  _link(String url) async {
    if (await canLaunch(url)) {
      launch(url);
    } else {
      throw 'Página não está acessivel!';
    }
  }
}

class CardHour extends StatefulWidget {
  @override
  _CardHourState createState() => _CardHourState();
}

class _CardHourState extends State<CardHour> {
  List<Departures> filteredUsers = List();

  List<Departures> _cards = List<Departures>();

  Future<List<Departures>> _schedule() async {
    var url = "$urlApi/departure/pagination/0?take=0&skip=0";
    var response = await http.get(url);
    var cards = List<Departures>();

    if (response.statusCode == 200) {
      var cardsJson = json.decode(response.body);
      for (var noteJson in cardsJson['departures']) {
        cards.add(Departures.fromJson(noteJson));
      }
    }
    return cards;
  }

  @override
  void initState() {
    _schedule().then((value) {
      setState(() {
        _cards.addAll(value);
        filteredUsers = _cards;
      });
    });
    super.initState();
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 1));

    setState(() {
      _schedule();
    });
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _schedule(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                  width: double.infinity,
                  height: 50,
                  alignment: Alignment.topCenter,
                  child: filteredUsers.isEmpty
                      ? CircularProgressIndicator(
                          strokeWidth: 5.0,
                        )
                      : null),
            );
          default:
            return Container(
              child: RefreshIndicator(
                onRefresh: _refresh,
                child: ListView.builder(
                  itemCount: filteredUsers.length,
                  itemBuilder: (context, index) {
                    return Container(
                      margin: EdgeInsets.all(8.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(8.0))),
                        child: InkWell(
                          child: Column(
                            children: <Widget>[
                              ListTileTheme(
                                contentPadding: EdgeInsets.all(10),
                                dense:
                                    true, //removes additional space vertically
                                child: Column(
                                  children: [
                                    ExpansionTile(
                                        title: Text(
                                          filteredUsers[index]
                                              .portRightName
                                              .toUpperCase(),
                                          style: TextStyle(
                                              fontSize: 18,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        subtitle: Text(
                                            filteredUsers[index].boatName,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500)),
                                        trailing: ClipRRect(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(8)),
                                          child: Container(
                                            width: 80,
                                            height: 80,
                                            color: Color(0xff5F9E29),
                                            child: Column(children: [
                                              Text('Preço',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                              Text(
                                                  'R\$${filteredUsers[index].price}',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                              Text('Reais',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 13,
                                                      fontWeight:
                                                          FontWeight.w600)),
                                            ]),
                                          ),
                                        ),
                                        children: [
                                          Container(
                                              padding: EdgeInsets.fromLTRB(
                                                  20, 0, 0, 0),
                                              child: Column(children: [
                                                ListTile(
                                                  contentPadding:
                                                      EdgeInsets.all(0),
                                                  title: Text(
                                                      filteredUsers[index]
                                                          .portLeftName,
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          color: Colors.black,
                                                          fontWeight: FontWeight.w600
                                                      )),
                                                  subtitle:
                                                      Text('Embarque às 8:00',
                                                          style: TextStyle(
                                                              fontSize: 13,
                                                              color: Colors.black,
                                                              fontWeight: FontWeight.w600
                                                          )
                                                      ),
                                                ),
                                                ListTile(
                                                  contentPadding:
                                                      EdgeInsets.all(0),
                                                  title: Text(
                                                      filteredUsers[index]
                                                          .portRightName,
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          color: Colors.black,
                                                          fontWeight: FontWeight.w600
                                                      )),
                                                  subtitle: Text(
                                                      'desembarque às ${"${DateTime.parse(filteredUsers[index].timeLeftPlan).hour}"
                                                          ":${DateTime.parse(filteredUsers[index].timeLeftPlan).minute}"
                                                          " Hrs"}',
                                                      style: TextStyle( fontSize: 13,
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.w600
                                                  )),
                                                ),
                                              ])),
                                        ]),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                            padding: EdgeInsets.all(15),
                                            child: Column(
                                              children: [
                                                Text(
                                                  'Partida',
                                                  style: TextStyle(
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                                Divider(
                                                  height: 4,
                                                ),
                                                Text(
                                                  "${DateTime.parse(filteredUsers[index].timeLeftPlan).hour}"
                                                  ":${DateTime.parse(filteredUsers[index].timeLeftPlan).minute}"
                                                  " Hrs",
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.w400),
                                                ),
                                              ],
                                            )),
                                        Container(
                                          padding: EdgeInsets.all(15),
                                          child: Column(
                                            children: [
                                              Text(
                                                'Chegada',
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                              Divider(
                                                height: 4,
                                              ),
                                              Text(
                                                "${DateTime.parse(filteredUsers[index].timeRightPlan).hour}"
                                                ":${DateTime.parse(filteredUsers[index].timeRightPlan).minute}"
                                                " Hrs",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16,
                                                    fontWeight:
                                                        FontWeight.w400),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            );
        }
      },
    );
  }
}
