import 'dart:async';
import 'package:travessia_sustentavel_mobile/src/page/Home_page/partidaProxima.dart';
import 'package:travessia_sustentavel_mobile/src/page/animation/loader_animator.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 4),
        () => Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (BuildContext context) {
          return PartidaProximaPage();
        })));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: new Color(0xff417618),
              gradient: LinearGradient(
                  colors: [new Color(0xff5F9E29), new Color(0xff417618)],
                  begin: Alignment.centerRight,
                  end: Alignment.centerLeft),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/images/splash-screen.png'),
              SizedBox(
                height: 40.0,
              ),
              Loading(
                radius: 18.0,
                dotRadius: 8.0,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
