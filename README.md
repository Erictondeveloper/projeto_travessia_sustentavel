# Travessia Sustentável Aplicação Mobile


## Travessia Sustentável Application
     Aplicativo para visualização de horários das lanchas da Marina do Davi desenvolvido pela Uninorte.

## Development version description:
     flutter: 1.20.4
     dart: 2.9.2

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
